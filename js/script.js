var osobe=new Array(new Osoba("Dusan","Radovanovic",1994,"Beograd"),new Osoba("Marko","Otasevic",1994,"Plav"),
	new Osoba("Nikola","Trajkovic",1999,"Beograd"),new Osoba("Filip","Nedovic",1989,"Bijelo Polje"));
function Osoba(ime, prezime, godinaRodjenja,mesto){
	this.ime=ime;
	this.prezime=prezime;
	this.godinaRodjenja=godinaRodjenja;
	this.mesto=mesto;
}

napraviTabelu(osobe);

function pretraziPoImenu(){
var element=document.getElementById("ime");
var ime=element.value;
element.value="";
napraviTabelu(nadjiOsobe(ime,"ime",false));

}

function pretraziPoPrezimenu(){
var element=document.getElementById("prezime");
var prezime=element.value;
element.value="";
napraviTabelu(nadjiOsobe(prezime,"prezime",false));
}

function pretraziPoGodini(){
var element=document.getElementById("godina");
var godina=element.value;
element.value="";
napraviTabelu(nadjiOsobe(godina,"godinaRodjenja",true));
}
function nadjiOsobe(tekst,atribut,godina){
	var trazeneOsobe=new Array();
	if(godina){
		for (var i = 0; i < osobe.length; i++) {
			if(osobe[i][atribut].toString().indexOf(tekst)!=-1){
				trazeneOsobe.push(osobe[i]);
			}
		}
	}else{
		for (var i = 0; i < osobe.length; i++) {
			if(osobe[i][atribut].indexOf(tekst)!=-1){
				trazeneOsobe.push(osobe[i]);
			}
		}
	}
	return trazeneOsobe;
}

function napraviTabelu(osobe){
	if(osobe.length==0) {
		document.write("Prazna tabela");
		return;
	}	
	document.write("<table border='solid'>");
	document.write("<th>Ime</th><th>Prezime</th><th>Godina rodjenja</th><th>Mesto</th>");
	for (var i = 0; i < osobe.length; i++) {
		document.write("<tr>");
		for(atribut in osobe[i]){
			document.write("<td>"+osobe[i][atribut]+"</td>");
		}
		document.write("</tr>");
	}
	document.write("</table>");
}